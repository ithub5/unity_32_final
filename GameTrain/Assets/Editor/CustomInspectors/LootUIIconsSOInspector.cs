using System;
using Loot.Data;
using UI.Loot.Data;
using UnityEditor;
using UnityEngine;
using Utils.Services;

namespace CustomInspectors
{
    [CustomEditor(typeof(LootUIIconsSO))]
    [CanEditMultipleObjects]
    public class LootUIIconsSOInspector : Editor
    {
        private SerializedProperty _lootGainTimeList;

        private void OnEnable()
        {
            SerializedProperty iterator = serializedObject.GetIterator();
            iterator.NextVisible(true);
            iterator.NextVisible(false);
            _lootGainTimeList = iterator.Copy();
            
            int lootTypeCount = Enum.GetValues(typeof(LootType)).Length;
            if (_lootGainTimeList.arraySize >= lootTypeCount) 
                return;
            
            _lootGainTimeList.arraySize = lootTypeCount;
            serializedObject.ApplyModifiedProperties();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EnumService.ForEach<LootType>(DrawLootTypeGainTime);
            serializedObject.ApplyModifiedProperties();
        }

        private void DrawLootTypeGainTime(LootType lootType)
        {
            SerializedProperty lootTypeGainTimeProperty = _lootGainTimeList.GetArrayElementAtIndex((int)lootType);
            string propertyLabel = $"{new GUIContent(lootType.ToString())} Gain Time";
            EditorGUILayout.PropertyField(lootTypeGainTimeProperty, new GUIContent(propertyLabel));
        }
    }
}