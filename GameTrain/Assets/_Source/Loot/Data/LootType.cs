namespace Loot.Data
{
    public enum LootType
    {
        Experience = 0,
        Equip = 1,
        Potion = 2,
        Summon = 3,
        Trash = 4
    }
}