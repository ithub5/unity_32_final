using System;
using System.Collections.Generic;
using Loot.Data;
using Utils.Services;

namespace Loot
{
    public class LootPile
    {
        private readonly Dictionary<LootType, int> _lootAmount = new Dictionary<LootType, int>();

        public event Action<LootType, int> OnLootUpdate;

        public LootPile() => EnumService.ForEach<LootType>(lootType => _lootAmount.Add(lootType, 0));

        public void Add(LootType lootType, int amount)
        {
            _lootAmount[lootType] += amount;
            
            OnLootUpdate?.Invoke(lootType, _lootAmount[lootType]);
        }

        public int this[LootType lootType] => _lootAmount[lootType];
    }
}