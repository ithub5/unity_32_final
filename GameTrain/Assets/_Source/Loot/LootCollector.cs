using MiniGames.Common.Collection;
using MiniGames.Common.Game;
using Utils.Extensions;

namespace Loot
{
    public class LootCollector
    {
        private readonly LootPile _lootPile;
        private readonly IMiniGame[] _games;

        public LootCollector(IGamesCollection gamesCollection, LootPile lootPile)
        {
            _lootPile = lootPile;
            _games = gamesCollection.Games;
            
            Bind();
        }

        private void Bind()
        {
            _games.ForEach(game => game.OnWin += _lootPile.Add);
        }
    }
}