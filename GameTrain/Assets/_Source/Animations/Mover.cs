using DG.Tweening;
using UnityEngine;
using Utils.Data;
using Zenject;

namespace Animations
{
    public class Mover : MonoBehaviour
    {
        [SerializeField] private Transform moveTarget;
        [SerializeField] private TransformRange moveRange;
        [SerializeField] private float moveSpeed;

        private float _oneSideMoveDuration;

        private Tween _moveToUpperRight;
        private Tween _moveToLowerLeft;

        [Inject]
        private void Init()
        {
            _oneSideMoveDuration = moveRange.Distance / moveSpeed;
            
            InitTween();
        }

        public Tween MoveToUpperRight()
        {
            _moveToUpperRight.Rewind();
            return _moveToUpperRight.Play();
        }
        
        public void SimpleMoveToUpperRight() => MoveToUpperRight();

        public Tween MoveToLowerLeft()
        {
            _moveToLowerLeft.Rewind();
            return _moveToLowerLeft.Play();
        }
        
        public void SimpleMoveToLowerLeft() => MoveToLowerLeft();

        public void Stop()
        {
            _moveToLowerLeft.Pause();
            _moveToUpperRight.Pause();
        }

        public void PingPongOn()
        {
            _moveToUpperRight.onComplete += SimpleMoveToLowerLeft;
            _moveToLowerLeft.onComplete += SimpleMoveToUpperRight;
        }

        public void PingPongOff()
        {
            _moveToUpperRight.onComplete -= SimpleMoveToLowerLeft;
            _moveToLowerLeft.onComplete -= SimpleMoveToUpperRight;
        }

        private void InitTween()
        {
            _moveToUpperRight = DOTween.To(() => moveTarget.transform.position, 
            (newValue) => moveTarget.transform.position = newValue, 
                moveRange.UpperRight.position, _oneSideMoveDuration).
                From(moveRange.LowerLeft.position).SetEase(Ease.Linear);

            _moveToUpperRight.SetAutoKill(false);
            _moveToUpperRight.Pause();
            
            _moveToLowerLeft = DOTween.To(() => moveTarget.transform.position,
                (newValue) => moveTarget.transform.position = newValue, 
                moveRange.LowerLeft.position, _oneSideMoveDuration).
                From(moveRange.UpperRight.position).SetEase(Ease.Linear);;
            
            _moveToLowerLeft.SetAutoKill(false);
            _moveToLowerLeft.Pause();
        }
    }
}