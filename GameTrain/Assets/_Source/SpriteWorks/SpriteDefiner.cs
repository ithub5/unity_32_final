using UnityEngine;

namespace SpriteWorks
{
    public class SpriteDefiner : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private Sprite undefinedSprite;
        [SerializeField] private Sprite correctSprite;
        [SerializeField] private Sprite wrongSprite;

        public void Undefined() => UpdateSprite(undefinedSprite);
        public void Correct() => UpdateSprite(correctSprite);
        public void Wrong() => UpdateSprite(wrongSprite);

        private void UpdateSprite(Sprite newSprite) => spriteRenderer.sprite = newSprite;
    }
}