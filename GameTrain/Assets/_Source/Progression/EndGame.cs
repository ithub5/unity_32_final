using System;
using Interactions.Click;
using UnityEngine;

namespace Progression
{
    public class EndGame : MonoBehaviour, IClickable
    {
        public event Action OnUse;

        public void ToggleOn() => gameObject.SetActive(true);

        public void ToggleOff() => gameObject.SetActive(false);

        public void StartedClick() { }

        public void PerformedClick() { }

        public void CanceledClick() => OnUse?.Invoke();
    }
}