using System;

namespace Progression
{
    public class Scorer
    {
        public int TargetValue { get; private set; }
        private int _currentValue;

        public event Action<int, int> OnScore;
        public event Action OnAchieveTarget;
        
        public void SetUp(int targetValue) => TargetValue = targetValue;

        public void Score(int value)
        {
            _currentValue = _currentValue + value < 0 ? 0 : _currentValue + value;
            OnScore?.Invoke(_currentValue, TargetValue);

            if (_currentValue < TargetValue)
                return;
            
            OnAchieveTarget?.Invoke();
        }

        public void Reset() => _currentValue = 0;
    }
}