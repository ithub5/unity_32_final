using UnityEngine;

namespace Progression
{
    public class ProgressBar : MonoBehaviour
    {
        [SerializeField] private Transform toScale;
        [SerializeField] private float minScale;
        [SerializeField] private float maxScale;

        [SerializeField] private bool alongX;
        [SerializeField] private bool alongY;

        public void ToggleOn() => gameObject.SetActive(true);
        
        public void ToggleOff() => gameObject.SetActive(false);
        
        public void UpdateView(int currentValue, int targetValue)
        {
            float newScale = maxScale * ((float)currentValue / (float)targetValue);
            if (newScale < minScale)
                newScale = minScale;
            else if (newScale > maxScale)
                newScale = maxScale;

            Vector3 resultScale = new Vector3(alongX ? newScale : toScale.localScale.x, alongY ? newScale : toScale.localScale.y, 1);
            toScale.localScale = resultScale;
        }

        public void Reset() => UpdateView(0, 1);
    }
}