using System;
using UI.Lose;
using UnityEngine;

namespace WInLose.Data
{
    [Serializable]
    public class WinLoseInstallerFields
    {
        [field: SerializeField] public LoseUIView LoseUIView { get; private set; }
    }
}