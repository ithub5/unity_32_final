using Core.GameState;
using UI.Lose;
using UnityEngine.SceneManagement;

namespace WInLose
{
    public class LoseState : IGameState
    {
        private readonly LoseUIView _uiView;

        public LoseState(LoseUIView uiView)
        {
            _uiView = uiView;
        }

        public void Enter()
        {
            _uiView.ToggleOn();

            _uiView.RestartBtn.onClick.AddListener(Exit);
            _uiView.RestartBtn.onClick.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex));
            
            _uiView.MenuBtn.onClick.AddListener(Exit);
            _uiView.MenuBtn.onClick.AddListener(() => SceneManager.LoadScene("MainMenu"));
        }

        public void Exit()
        {
            _uiView.RestartBtn.onClick.RemoveAllListeners();
            _uiView.MenuBtn.onClick.RemoveAllListeners();
            
            _uiView.ToggleOff();
        }
    }
}