using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Utils.Data
{
    [Serializable]
    public class IntRange
    {
        [field: SerializeField] public int Min { get; private set; }
        [field: SerializeField] public int Max { get; private set; }

        public int RandomInRange() => Random.Range(Min, Max);
    }
}