using UnityEngine;

namespace Utils.Data
{
    public class TransformRange : MonoBehaviour
    {
        [field: SerializeField] public Transform LowerLeft { get; private set; }
        [field: SerializeField] public Transform UpperRight { get; private set; }

        public Vector2 Middle => (UpperRight.position - LowerLeft.position) / 2;
        public float Distance => Vector2.Distance(UpperRight.position, LowerLeft.position);

        public Vector2 RandomInRange() =>
            new Vector2(
                Random.Range(LowerLeft.position.x, UpperRight.position.x),
                Random.Range(LowerLeft.position.y, UpperRight.position.y)
                );
    }
}