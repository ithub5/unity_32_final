using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Utils.Data
{
    [Serializable]
    public class FloatRange
    {
        [field: SerializeField] public float Min { get; private set; }
        [field: SerializeField] public float Max { get; private set; }

        public float RandomInRange() => Random.Range(Min, Max);
    }
}