using DG.Tweening;

namespace Utils.Services
{
    public static class DOTweenService
    {
        public static Sequence GetAutoKillOffSequence(bool pause = true)
        {
            Sequence newSequence = DOTween.Sequence();
            newSequence.SetAutoKill(false);
            if (pause)
                newSequence.Pause();

            return newSequence;
        } 
    }
}