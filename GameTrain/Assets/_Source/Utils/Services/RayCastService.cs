using UnityEngine;

namespace Utils.Services
{
    public static class RayCastService
    {
        public static bool RayCastFromScreen(Vector2 targetScreenPos, LayerMask layerMask, out RaycastHit2D result)
        {
            Vector2 targetWorldPos = CameraService.ScreenToWorldPoint(targetScreenPos);
            result = Physics2D.Raycast(targetWorldPos, Vector2.zero, 1f, layerMask);

            return result.collider != null;
        }
    }
}