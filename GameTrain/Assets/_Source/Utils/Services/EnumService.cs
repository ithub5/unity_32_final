using System;
using System.Collections.Generic;

namespace Utils.Services
{
    public static class EnumService
    {
        public static IEnumerable<T> Values<T>() where T : Enum => Enum.GetValues(typeof(T)) as T[];
        
        public static void ForEach<T>(Action<T> action) where T : Enum
        {
            foreach (var intValue in Enum.GetValues(typeof(T)))
                action((T)intValue);
        }
    }
}