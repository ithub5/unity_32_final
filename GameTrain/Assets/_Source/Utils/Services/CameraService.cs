using UnityEngine;

namespace Utils.Services
{
    public static class CameraService
    {
        public static Vector2 ScreenToWorldPoint(Vector2 screenPos) =>
            Camera.main.ScreenToWorldPoint(screenPos);
    }
}