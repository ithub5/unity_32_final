using System;
using System.Collections.Generic;
using System.Linq;

namespace Utils.Extensions
{
    public static class EnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            foreach (var item in collection)
                action(item);
        }

        public static int Random<T>(this IEnumerable<T> collection, out T result)
        {
            int randomIndex = UnityEngine.Random.Range(0, collection.Count());

            result = collection.ElementAt(randomIndex);
            return randomIndex;
        }
        
        public static T Random<T>(this IEnumerable<T> collection)
        {
            collection.Random(out T result);
            return result;
        }
    }
}