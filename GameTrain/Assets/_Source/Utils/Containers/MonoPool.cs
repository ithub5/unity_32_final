using System.Collections.Generic;
using UnityEngine;
using Utils.Containers.Data;

namespace Utils.Containers
{
    public class MonoPool<T> where T : Component
    {
        private readonly MonoPoolConfigSO<T> _configSO;
        private readonly Transform _holder;
        
        private readonly Queue<T> _items = new Queue<T>();

        public MonoPool(MonoPoolConfigSO<T> configSO)
        {
            _configSO = configSO;
            _holder = new GameObject($"{_configSO.Prefab.name}Holder(MonoPool)").transform;
            
            InitItems();
        }

        private void InitItems()
        {
            for (int i = 0; i < _configSO.Capacity; i++)
                AddItem();
        }

        public T Get()
        {
            if (_items.Count > 0)
                return _items.Dequeue();
            
            AddItem();
            return Get();
        }

        public void Return(T item)
        {
            if (_items.Contains(item))
                return;
            
            _items.Enqueue(item);
        }

        private void AddItem()
        {
            T newItem = Object.Instantiate(_configSO.Prefab.gameObject, _holder).GetComponent<T>();
            newItem.gameObject.SetActive(false);

            _items.Enqueue(newItem);
        }
    }
}