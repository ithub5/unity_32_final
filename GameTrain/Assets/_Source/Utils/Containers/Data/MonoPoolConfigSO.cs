using UnityEngine;

namespace Utils.Containers.Data
{
    public abstract class MonoPoolConfigSO<T> : ScriptableObject
    {
        [field: SerializeField] public T Prefab { get; private set; }
        [field: SerializeField] public int Capacity { get; private set; }
    }
}