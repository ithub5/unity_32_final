using System.Collections.Generic;
using UnityEngine;
using Utils.Containers;
using Utils.Data;

namespace Spawn
{
    public class Spawner<T> where T : Component, ISpawnable<T>
    {
        private readonly MonoPool<T> _pool;
        private readonly TransformRange _spawnRange;

        private readonly List<T> _active = new List<T>();

        public Spawner(MonoPool<T> pool, TransformRange spawnRange)
        {
            _pool = pool;
            _spawnRange = spawnRange;
        }

        public T Spawn()
        {
            T newItem = _pool.Get();
            
            newItem.PlaceSelf(_spawnRange);
            
            newItem.Spawn();
            newItem.OnDespawn += Return;

            _active.Add(newItem);
            return newItem;
        }

        private void Return(T item)
        {
            item.OnDespawn -= Return;

            _active.Remove(item);
            _pool.Return(item);
        }

        public void DespawnActive()
        {
            for (int i = _active.Count - 1; i >= 0; i--)
                _active[i].Despawn();
        }
    }
}