using System;
using UnityEngine;
using Utils.Data;

namespace Spawn
{
    public interface ISpawnable<T> where T : Component
    {
        public event Action<T> OnDespawn;

        public void Spawn();
        public void Despawn();
        
        public void PlaceSelf(TransformRange placeRange);
    }
}