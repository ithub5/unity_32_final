using Core.GameState;
using Loot;
using Loot.Data;
using QuestSystem.Data;
using WInLose;

namespace QuestSystem
{
    public class QuestManager
    {
        private readonly Quest _quest;
        private readonly QuestLootAmountSO _questLootAmountSO;
        private readonly QuestLootGainTimeSO _questLootGainTimeSO;

        private readonly LootPile _lootPile;

        private readonly GameStateMachine _gameStateMachine;

        public QuestManager(Quest quest, QuestLootAmountSO questLootAmountSO, QuestLootGainTimeSO questLootGainTimeSO, 
            LootPile lootPile, GameStateMachine gameStateMachine)
        {
            _quest = quest;
            _questLootAmountSO = questLootAmountSO;
            _questLootGainTimeSO = questLootGainTimeSO;
            _lootPile = lootPile;
            _gameStateMachine = gameStateMachine;
        }

        public void Start()
        {
            Bind();
            
            ReGenerateQuest();
        }

        private void Bind()
        {
            _quest.OnComplete += ReGenerateQuest;
            _quest.OnLose += Lose; 
                
            _lootPile.OnLootUpdate += CheckLoot;
        }
        
        private void CheckLoot(LootType _, int __) => _quest.CheckLoot(_lootPile);

        private void ReGenerateQuest() => _quest.GenerateValues(_questLootAmountSO, _questLootGainTimeSO);

        private void Lose() => _gameStateMachine.ToState<LoseState>();
    }
}