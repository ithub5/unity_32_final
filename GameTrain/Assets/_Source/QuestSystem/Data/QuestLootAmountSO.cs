using UnityEngine;
using Utils.Data;

namespace QuestSystem.Data
{
    [CreateAssetMenu(menuName = "SO/QuestSystem/QuestLootAmountSO", fileName = "NewQuestLootAmountSO")]
    public class QuestLootAmountSO : ScriptableObject
    {
        [field: SerializeField] public IntRange DifferentLootTypesAmount { get; private set; }
        [field: SerializeField] public IntRange OneLootTypeAmount { get; private set; }
    }
}