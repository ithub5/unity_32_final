using System.Collections.Generic;
using Loot.Data;
using UnityEngine;

namespace QuestSystem.Data
{
    [CreateAssetMenu(menuName = "SO/QuestSystem/QuestLootGainTimeSO", fileName = "NewQuestLootGainTimeSO")]
    public class QuestLootGainTimeSO : ScriptableObject
    {
        [SerializeField] private List<float> lootGainTime;

        public float this[LootType lootType] => lootGainTime[(int)lootType];
    }
}