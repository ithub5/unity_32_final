using System;
using UnityEngine;

namespace QuestSystem.Data
{
    [Serializable]
    public class QuestSystemInstallerFields
    {
        [field: SerializeField] public QuestLootAmountSO QuestLootAmount { get; private set; }
        [field: SerializeField] public QuestLootGainTimeSO QuestLootGainTimeSO { get; private set; }
    }
}