using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using DG.Tweening;
using Loot;
using Loot.Data;
using QuestSystem.Data;
using Utils.Extensions;
using Utils.Services;

namespace QuestSystem
{
    public class Quest
    {
        private readonly Dictionary<LootType, int> _lootNeeded = new Dictionary<LootType, int>();
        private ReadOnlyDictionary<LootType, int> _lootNeededView;

        private Sequence _timeCounterSequence;

        public event Action OnComplete;
        public event Action OnLose;

        public event Action OnNewTarget;

        public ReadOnlyDictionary<LootType, int> LootNeeded => _lootNeededView ??= new ReadOnlyDictionary<LootType, int>(_lootNeeded);
        public float TimeToComplete { get; private set; }
        public int CompletedAmount { get; private set; }

        public void Start() => _timeCounterSequence.Play();

        public void CheckLoot(LootPile lootPile)
        {
            if (_lootNeeded.Any(lootTypeNAmount => lootPile[lootTypeNAmount.Key] < lootTypeNAmount.Value))
                return;

            foreach (var lootTypeNeeded in _lootNeeded)
                lootPile.Add(lootTypeNeeded.Key, -lootTypeNeeded.Value);
            
            Complete();
        }
        
        private void Complete()
        {
            CompletedAmount++;
            OnComplete?.Invoke();
        }

        private void Lose() => OnLose?.Invoke();

        public void GenerateValues(QuestLootAmountSO questLootAmountSO, QuestLootGainTimeSO questLootGainTimeSO)
        {
            _lootNeeded.Clear();
            TimeToComplete = 0;

            List<LootType> lootTypes = EnumService.Values<LootType>().ToList();
            for (int i = 0; i < questLootAmountSO.DifferentLootTypesAmount.RandomInRange(); i++)
            {
                lootTypes.RemoveAt(lootTypes.Random(out LootType lootType));
                
                _lootNeeded.Add(lootType, questLootAmountSO.OneLootTypeAmount.RandomInRange());
                TimeToComplete += questLootGainTimeSO[lootType];
            }
            
            SetTimeCounter();
            
            OnNewTarget?.Invoke();
        }

        private void SetTimeCounter()
        {
            _timeCounterSequence?.Kill();
            _timeCounterSequence = DOTween.Sequence();
            _timeCounterSequence.Pause();

            _timeCounterSequence.AppendInterval(TimeToComplete);
            _timeCounterSequence.AppendCallback(Lose);
        }
    }
}