using System;
using System.Collections.Generic;
using Core.GameState.Signals;
using Core.GameState.StatesHolder;

namespace Core.GameState
{
    public class GameStateMachine
    {
        private readonly Dictionary<Type, IGameState> _gameStates;

        private IGameState _currentState;

        private readonly ChangeGameStateSignal _changeGameStateSignal;
        
        public GameStateMachine(IGameStatesHolder gameStatesHolder)
        {
            _gameStates = gameStatesHolder.GameStates;

            _changeGameStateSignal = deVoid.Utils.Signals.Get<ChangeGameStateSignal>();
        }

        public void Bind() => _changeGameStateSignal.AddListener(ToState);

        public void Expose() => _changeGameStateSignal.RemoveListener(ToState);

        public void ToState<T>() where T : IGameState
        {
            
            Type newStateType = typeof(T);
            ToState(newStateType);
        }

        public void ToState(Type newStateType)
        {
            if (!_gameStates.ContainsKey(newStateType))
                return;
            
            _currentState?.Exit();
            _currentState = _gameStates[newStateType];
            _currentState.Enter();
        }
    }
}