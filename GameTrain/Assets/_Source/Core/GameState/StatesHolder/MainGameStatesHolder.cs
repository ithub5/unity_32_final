using System;
using System.Collections.Generic;
using Hub;
using MiniGames.EquipGame;
using MiniGames.ExperienceGame;
using MiniGames.PotionGame;
using MiniGames.SummonGame;
using MiniGames.TrashGame;
using WInLose;

namespace Core.GameState.StatesHolder
{
    public class MainGameStatesHolder : IGameStatesHolder
    {
        public Dictionary<Type, IGameState> GameStates { get; }
        
        public MainGameStatesHolder(HubState hubState, ExperienceGameState experienceGameState, EquipGameState equipGameState,
            PotionGameState potionGameState, SummonGameState summonGameState, TrashGameState trashGameState, LoseState loseState)
        {
            GameStates = new Dictionary<Type, IGameState>();
            
            GameStates.Add(typeof(HubState), hubState);
            GameStates.Add(typeof(ExperienceGameState), experienceGameState);
            GameStates.Add(typeof(EquipGameState), equipGameState);
            GameStates.Add(typeof(PotionGameState), potionGameState);
            GameStates.Add(typeof(SummonGameState), summonGameState);
            GameStates.Add(typeof(TrashGameState), trashGameState);
            GameStates.Add(typeof(LoseState), loseState);
        }
    }
}