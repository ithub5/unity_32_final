using System;
using System.Collections.Generic;

namespace Core.GameState.StatesHolder
{
    public interface IGameStatesHolder
    {
        public Dictionary<Type, IGameState> GameStates { get; }
    }
}