using System;
using deVoid.Utils;

namespace Core.GameState.Signals
{
    public class ChangeGameStateSignal : ASignal<Type> { }
}