using Core.GameState;
using Hub;
using QuestSystem;
using UnityEngine;
using Zenject;

namespace Core
{
    public class Bootstrapper : MonoBehaviour
    {
        private GameStateMachine _gameStateMachine;
        private QuestManager _questManager;

        [Inject]
        private void Init(GameStateMachine gameStateMachine, QuestManager questManager)
        {
            _gameStateMachine = gameStateMachine;
            _questManager = questManager;
        }

        private void Start()
        {
            _gameStateMachine.Bind();
            _gameStateMachine.ToState<HubState>();
            
            _questManager.Start();
        }
    }
}