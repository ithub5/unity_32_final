using UI.MainMenu;
using UI.MainMenu.Data;
using UnityEngine;
using Zenject;

namespace Core.Installers
{
    public class MainMenuInstaller : MonoInstaller
    {
        [SerializeField] private MainMenuUIInstallerFields mainMenuUI;
        
        public override void InstallBindings()
        {
            MainMenuUI();
        }

        private void MainMenuUI()
        {
            Container.Bind<MainMenuUIView>().FromInstance(mainMenuUI.MainMenuUIView).AsSingle().NonLazy();
            Container.Bind<MainMenuUIController>().AsSingle().NonLazy();
        }
    }
}