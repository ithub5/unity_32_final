using Core.GameState;
using Core.GameState.StatesHolder;
using Hub;
using Hub.Data;
using Hub.Switcher;
using Hub.Switcher.Switches;
using Input;
using Interactions;
using Interactions.Click;
using Interactions.Data;
using Interactions.Drag;
using Loot;
using MiniGames.Common.Collection;
using MiniGames.EquipGame;
using MiniGames.EquipGame.Data;
using MiniGames.ExperienceGame;
using MiniGames.ExperienceGame.Data;
using MiniGames.PotionGame;
using MiniGames.PotionGame.Data;
using MiniGames.SummonGame;
using MiniGames.SummonGame.Data;
using MiniGames.TrashGame;
using MiniGames.TrashGame.Data;
using Progression;
using QuestSystem;
using QuestSystem.Data;
using Spawn;
using UI.Loot;
using UI.Loot.Data;
using UI.Lose;
using UI.Quest;
using UI.Quest.Data;
using UnityEngine;
using Utils.Containers;
using WInLose;
using WInLose.Data;
using Zenject;
using ProgressBar = Progression.ProgressBar;

namespace Core.Installers
{
    public class GamePlayInstaller : MonoInstaller
    {
        [SerializeField] private InteractionConfigSO interactionConfigSO;

        [SerializeField] private HubInstallerFields hub;
        [SerializeField] private ProgressBar miniGamesProgressBar;
        [SerializeField] private EndGame endGame;
        [SerializeField] private ExperienceGameInstallerFields experienceGame;
        [SerializeField] private EquipGameInstallerFields equipGame;
        [SerializeField] private PotionGameInstallerFields potionGame;
        [SerializeField] private SummonGameInstallerFields summonGame;
        [SerializeField] private TrashGameInstallerFields trashGame;
        [SerializeField] private QuestSystemInstallerFields questSystem;
        [SerializeField] private WinLoseInstallerFields winLose;
        [SerializeField] private LootUIInstallerFields lootUI;
        [SerializeField] private QuestSystemUIInstallerFields questSystemUI;


        public override void InstallBindings()
        {
            Interaction();
            Hub();
            
            Container.Bind<EndGame>().FromInstance(endGame).AsCached().NonLazy();
            MiniGamesCommon();
            ExperienceGame();
            EquipGame();
            PotionGame();
            SummonGame();
            TrashGame();
            Loot();
            QuestSystem();
            WinLose();

            LootUI();
            QuestSystemUI();

            Container.Bind<IGameStatesHolder>().To<MainGameStatesHolder>().AsSingle().NonLazy();
            Container.Bind<GameStateMachine>().AsSingle().NonLazy();
        }

        private void Interaction()
        {
            MainActions mainActions = new MainActions();
            mainActions.Enable();
            Container.Bind<MainActions>().FromInstance(mainActions).AsCached().NonLazy();

            Container.Bind<LayerMask>().WithId(ZInteractionIDs.ClickableMask).FromInstance(interactionConfigSO.ClickableMask).AsCached().NonLazy();
            Container.Bind<LayerMask>().WithId(ZInteractionIDs.SwipeableMask).FromInstance(interactionConfigSO.SwipeableMask).AsCached().NonLazy();
            
            Container.Bind<Clicker>().AsSingle().NonLazy();
            Container.Bind<Dragger>().AsSingle().NonLazy();

            Container.Bind<Interacter>().AsSingle().NonLazy();
        }
        
        private void Hub()
        {
            Container.Bind<AGameSwitch[]>().FromInstance(hub.ToGames).AsCached().NonLazy();
            
            Container.Bind<GameStateSwitcher>().AsSingle().NonLazy();
            
            Container.Bind<HubState>().AsSingle().NonLazy();
        }

        private void MiniGamesCommon()
        {
            Container.Bind<Scorer>().AsSingle().NonLazy();
            Container.Bind<ProgressBar>().FromInstance(miniGamesProgressBar).AsSingle().NonLazy();
        }

        private void EquipGame()
        {
            Container.Bind<EquipGameConfigSO>().FromInstance(equipGame.ConfigSO).AsSingle().NonLazy();

            MonoPool<HitPoint> hitPointPool = new MonoPool<HitPoint>(equipGame.HitPointPoolConfigSO);
            Container.Bind<Spawner<HitPoint>>().FromInstance(new Spawner<HitPoint>(hitPointPool, equipGame.PlayArea));

            Container.Bind<EquipGame>().AsSingle().NonLazy();
            Container.Bind<EquipGameState>().AsSingle().NonLazy();
        }

        private void ExperienceGame()
        {
            Container.Bind<ExperienceGameConfigSO>().FromInstance(experienceGame.ConfigSO).AsSingle().NonLazy();

            Container.Bind<ExperienceBottle>().FromInstance(experienceGame.ExperienceBottle).AsSingle().NonLazy();
            
            MonoPool<ExperiencePart> experiencePartPool = new MonoPool<ExperiencePart>(experienceGame.ExperiencePartPoolConfigSO);
            Container.Bind<Spawner<ExperiencePart>>().FromInstance(new Spawner<ExperiencePart>(experiencePartPool, experienceGame.PlayArea));

            Container.Bind<ExperienceGame>().AsSingle().NonLazy();
            Container.Bind<ExperienceGameState>().AsSingle().NonLazy();
        }

        private void PotionGame()
        {
            Container.Bind<PotionGameConfigSO>().FromInstance(potionGame.ConfigSO).AsSingle().NonLazy();

            Container.Bind<Brewer>().FromInstance(potionGame.Brewer).AsSingle().NonLazy();

            MonoPool<PotionPart> potionPartPool = new MonoPool<PotionPart>(potionGame.PotionPartPoolConfigSO);
            Container.Bind<Spawner<PotionPart>>().FromInstance(new Spawner<PotionPart>(potionPartPool, potionGame.PlayArea));

            Container.Bind<PotionGame>().AsSingle().NonLazy();
            Container.Bind<PotionGameState>().AsSingle().NonLazy();
        }
        private void SummonGame()
        {
            Container.Bind<SummonGameConfigSO>().FromInstance(summonGame.ConfigSO).AsSingle().NonLazy();
            
            Container.Bind<Key[]>().FromInstance(summonGame.Keys).AsSingle().NonLazy();
            Container.Bind<LockNView[]>().FromInstance(summonGame.LocksNViews).AsSingle().NonLazy();

            Container.Bind<SummonGame>().AsSingle().NonLazy();
            Container.Bind<SummonGameState>().AsSingle().NonLazy();
        }

        private void TrashGame()
        {
            Container.Bind<TrashGameConfigSO>().FromInstance(trashGame.ConfigSO).AsSingle().NonLazy();
            Container.Bind<Trash>().FromInstance(trashGame.Trash).AsSingle().NonLazy();

            Container.Bind<TrashGame>().AsSingle().NonLazy();
            Container.Bind<TrashGameState>().AsSingle().NonLazy();
        }

        private void Loot()
        {
            Container.Bind<LootPile>().AsCached().NonLazy();

            Container.Bind<IGamesCollection>().To<MainGamesCollection>().AsSingle().NonLazy();
            Container.Bind<LootCollector>().AsSingle().NonLazy();
        }

        private void QuestSystem()
        {
            Container.Bind<QuestLootAmountSO>().FromInstance(questSystem.QuestLootAmount).AsSingle().NonLazy();
            Container.Bind<QuestLootGainTimeSO>().FromInstance(questSystem.QuestLootGainTimeSO).AsSingle().NonLazy();

            Container.Bind<Quest>().AsSingle().NonLazy();
            Container.Bind<QuestManager>().AsSingle().NonLazy();
        }

        private void LootUI()
        {
            Container.Bind<LootPileUIView>().FromInstance(lootUI.LootPileUIView).AsSingle().NonLazy();
            Container.Bind<LootPileUIController>().AsSingle().NonLazy();
        }

        private void QuestSystemUI()
        {
            Container.Bind<QuestUIView>().FromInstance(questSystemUI.QuestUIView).AsSingle().NonLazy();
            Container.Bind<QuestUIController>().AsSingle().NonLazy();
        }

        private void WinLose()
        {
            Container.Bind<LoseUIView>().FromInstance(winLose.LoseUIView).AsSingle().NonLazy();
            Container.Bind<LoseState>().AsSingle().NonLazy();
        }
    }
}
