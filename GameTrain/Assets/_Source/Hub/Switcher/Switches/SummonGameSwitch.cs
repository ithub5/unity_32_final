using Interactions.Click;
using MiniGames.SummonGame;

namespace Hub.Switcher.Switches
{
    public class SummonGameSwitch : AGameSwitch, IClickable
    {
        public void StartedClick() { }
        public void PerformedClick() { }
        public void CanceledClick() => Use<SummonGameState>();
    }
}