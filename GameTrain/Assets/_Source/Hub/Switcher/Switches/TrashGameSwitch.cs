using Interactions.Click;
using MiniGames.TrashGame;

namespace Hub.Switcher.Switches
{
    public class TrashGameSwitch : AGameSwitch, IClickable
    {
        public void StartedClick() { }
        public void PerformedClick() { }
        public void CanceledClick() => Use<TrashGameState>();
    }
}