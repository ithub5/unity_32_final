using Interactions.Click;
using MiniGames.PotionGame;

namespace Hub.Switcher.Switches
{
    public class PotionGameSwitch : AGameSwitch, IClickable
    {
        public void StartedClick() { }

        public void PerformedClick() { }

        public void CanceledClick() => Use<PotionGameState>();
    }
}