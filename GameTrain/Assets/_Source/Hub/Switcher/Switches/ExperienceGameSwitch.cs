using Interactions.Click;
using MiniGames.ExperienceGame;

namespace Hub.Switcher.Switches
{
    public class ExperienceGameSwitch : AGameSwitch, IClickable
    {
        public void StartedClick() { }

        public void PerformedClick() { }

        public void CanceledClick() => Use<ExperienceGameState>();
    }
}