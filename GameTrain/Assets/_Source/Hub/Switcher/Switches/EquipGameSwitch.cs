using Interactions.Click;
using MiniGames.EquipGame;

namespace Hub.Switcher.Switches
{
    public class EquipGameSwitch : AGameSwitch, IClickable
    {
        public void StartedClick() { }
        public void PerformedClick() { }
        public void CanceledClick() => Use<EquipGameState>();
    }
}