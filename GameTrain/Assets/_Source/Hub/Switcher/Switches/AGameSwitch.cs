using System;
using Core.GameState;
using UnityEngine;

namespace Hub.Switcher.Switches
{
    public abstract class AGameSwitch : MonoBehaviour
    {
        public event Action<Type> OnUse;

        protected void Use<T>() where T : IGameState => OnUse?.Invoke(typeof(T));
    }
}