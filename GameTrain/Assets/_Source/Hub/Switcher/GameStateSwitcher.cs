using System;
using Core.GameState.Signals;
using Hub.Switcher.Switches;

namespace Hub.Switcher
{
    public class GameStateSwitcher
    {
        private readonly AGameSwitch[] _toGames;

        private readonly ChangeGameStateSignal _changeGameStateSignal;

        public GameStateSwitcher(AGameSwitch[] toGames)
        {
            _toGames = toGames;

            _changeGameStateSignal = deVoid.Utils.Signals.Get<ChangeGameStateSignal>();
        }

        public void Bind()
        {
            foreach (var toGame in _toGames)
                toGame.OnUse += Switch;
        }

        public void Expose()
        {
            foreach (var toGame in _toGames)
                toGame.OnUse -= Switch;
        }

        private void Switch(Type newGameState) => _changeGameStateSignal.Dispatch(newGameState);
    }
}