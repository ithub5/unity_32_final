using System;
using Hub.Switcher.Switches;
using UnityEngine;

namespace Hub.Data
{
    [Serializable]
    public class HubInstallerFields
    {
        [field: SerializeField] public AGameSwitch[] ToGames { get; private set; }
    }
}