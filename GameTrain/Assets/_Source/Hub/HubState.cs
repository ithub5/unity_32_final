using Core.GameState;
using Hub.Switcher;
using Interactions;

namespace Hub
{
    public class HubState : IGameState
    {
        private readonly GameStateSwitcher _gameStateSwitcher;
        private readonly Interacter _interacter;

        public HubState(GameStateSwitcher gameStateSwitcher, Interacter interacter)
        {
            _gameStateSwitcher = gameStateSwitcher;
            _interacter = interacter;
        }

        public void Enter()
        {
            _interacter.BindClicker();
            _gameStateSwitcher.Bind();
        }

        public void Exit()
        {
            _gameStateSwitcher.Expose();
            _interacter.ExposeClicker();
        }
    }
}