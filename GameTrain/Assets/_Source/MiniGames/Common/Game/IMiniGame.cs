using System;
using Loot.Data;

namespace MiniGames.Common.Game
{
    public interface IMiniGame
    {
        public event Action<LootType, int> OnWin;

        public void Start();
        public void End();
    }
}