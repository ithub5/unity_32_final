using System;
using Loot.Data;
using MiniGames.Common.Data;
using Progression;

namespace MiniGames.Common.Game
{
    public class AMiniGame : IMiniGame
    {
        protected readonly AMiniGameConfigSO configSO;
        protected readonly Scorer scorer;
        private readonly ProgressBar _progressBar;

        public event Action<LootType, int> OnWin;

        public AMiniGame(AMiniGameConfigSO configSO, Scorer scorer, ProgressBar progressBar)
        {
            this.configSO = configSO;
            this.scorer = scorer;
            _progressBar = progressBar;
        }

        public virtual void Start()
        {
            _progressBar.ToggleOn();

            scorer.SetUp(configSO.TargetScore.RandomInRange());
            scorer.OnScore += _progressBar.UpdateView;
            scorer.OnAchieveTarget += Win;
        }

        public virtual void End()
        {
            _progressBar.ToggleOff();
            _progressBar.Reset();

            scorer.Reset();
            scorer.OnScore -= _progressBar.UpdateView;
            scorer.OnAchieveTarget -= Win;
        }

        protected virtual void Win() => OnWin?.Invoke(configSO.LootType, configSO.WinReward);

        protected virtual void ScoreUp() => scorer.Score(configSO.ScoreStep);
    }
}