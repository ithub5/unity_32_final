using Core.GameState;
using Core.GameState.Signals;
using deVoid.Utils;
using Hub;
using Loot.Data;
using Progression;

namespace MiniGames.Common.Game
{
    public abstract class AMiniGameState : IGameState
    {
        private readonly IMiniGame _game;
        private readonly EndGame _endGame;

        private readonly ChangeGameStateSignal _changeGameStateSignal;

        protected AMiniGameState(IMiniGame game, EndGame endGame)
        {
            _game = game;
            _endGame = endGame;

            _changeGameStateSignal = Signals.Get<ChangeGameStateSignal>();
        }

        public virtual void Enter()
        {
            _game.Start();
            
            _game.OnWin += ToHub;
            _endGame.ToggleOn();
            _endGame.OnUse += ToHub;
        }

        private void ToHub(LootType _, int __) => ToHub();
        protected virtual void ToHub() => _changeGameStateSignal.Dispatch(typeof(HubState));

        public virtual void Exit()
        {
            _game.OnWin -= ToHub;
            _endGame.OnUse -= ToHub;
            _endGame.ToggleOff();
            
            _game.End();
        }
    }
}