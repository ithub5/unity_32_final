using MiniGames.Common.Game;

namespace MiniGames.Common.Collection
{
    public class MainGamesCollection : IGamesCollection
    {
        public IMiniGame[] Games { get; }

        public MainGamesCollection(EquipGame.EquipGame equipGame, ExperienceGame.ExperienceGame experienceGame, 
            PotionGame.PotionGame potionGame, SummonGame.SummonGame summonGame, TrashGame.TrashGame trashGame)
        {
            Games = new IMiniGame[] { equipGame, experienceGame, potionGame, summonGame, trashGame };
        }

    }
}