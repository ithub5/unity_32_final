using MiniGames.Common.Game;

namespace MiniGames.Common.Collection
{
    public interface IGamesCollection
    {
        public IMiniGame[] Games { get; }
    }
}