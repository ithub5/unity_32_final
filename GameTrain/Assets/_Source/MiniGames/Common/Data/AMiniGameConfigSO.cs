using Loot.Data;
using UnityEngine;
using Utils.Data;

namespace MiniGames.Common.Data
{
    public abstract class AMiniGameConfigSO : ScriptableObject
    {
        [field: SerializeField] public int WinReward { get; private set; }
        [field: SerializeField] public LootType LootType { get; private set; }
        [field: SerializeField] public IntRange TargetScore { get; private set; }
        [field: SerializeField] public int ScoreStep { get; private set; }
    }
}