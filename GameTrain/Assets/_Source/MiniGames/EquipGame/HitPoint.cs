using System;
using DG.Tweening;
using Interactions.Click;
using Spawn;
using UnityEngine;
using Utils.Data;
using Utils.Services;

namespace MiniGames.EquipGame
{
    public class HitPoint : MonoBehaviour, IClickable, ISpawnable<HitPoint>
    {
        [SerializeField] private float dieDelay;

        [SerializeField] private CircleCollider2D placeCollider;
        [SerializeField] private LayerMask placeMask;

        [SerializeField] private float normalScale;
        [SerializeField] private float hitScale;
        [SerializeField] private float appearTime;
        [SerializeField] private float disappearTime;
        [SerializeField] private float hitScaleTime;

        private bool _interactable;
        
        private Sequence _spawnSequence;
        private Sequence _despawnSequence;
        private Sequence _hitSequence;

        public event Action OnHit;
        public event Action OnDie;

        public event Action<HitPoint> OnDespawn;
        
        private void Awake() => InitSequence();

        public void Spawn() => _spawnSequence.Restart();

        public void Despawn() => _despawnSequence.Restart();

        public void StartedClick()
        {
            if (!_interactable)
                return;

            _hitSequence.Restart();
        }

        public void PerformedClick() { }
        public void CanceledClick() { }

        public void PlaceSelf(TransformRange placeRange)
        {
            transform.position = placeRange.RandomInRange();
            
            if (Physics2D.OverlapCircle(placeCollider.transform.position, placeCollider.radius, placeMask) == null)
                return;
            
            PlaceSelf(placeRange);
        }
        
        private void InitSequence()
        {
            _spawnSequence = DOTweenService.GetAutoKillOffSequence();

            _spawnSequence.AppendCallback(() => gameObject.SetActive(true));
            _spawnSequence.Append(transform.DOScale(normalScale, appearTime).From(0f));
            _spawnSequence.AppendCallback(() => _interactable = true);
            _spawnSequence.AppendInterval(dieDelay);
            _spawnSequence.AppendCallback(() => _interactable = false);
            _spawnSequence.AppendCallback(() => OnDie?.Invoke());
            _spawnSequence.AppendCallback(Despawn);

            _despawnSequence = DOTweenService.GetAutoKillOffSequence();
            
            _despawnSequence.Append(transform.DOScale(0f, disappearTime));
            _despawnSequence.AppendCallback(() => gameObject.SetActive(false));
            _despawnSequence.AppendCallback(() => OnDespawn?.Invoke(this));

            _hitSequence = DOTweenService.GetAutoKillOffSequence();

            _hitSequence.AppendCallback(() => _spawnSequence.Pause());
            _hitSequence.AppendCallback(() => _interactable = false);
            _hitSequence.Append(transform.DOScale(hitScale, hitScaleTime));
            _hitSequence.AppendCallback(() => OnHit?.Invoke());
            _hitSequence.AppendCallback(Despawn);
        }
    }
}