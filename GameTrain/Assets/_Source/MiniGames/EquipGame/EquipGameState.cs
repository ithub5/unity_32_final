using Interactions;
using MiniGames.Common.Game;
using Progression;

namespace MiniGames.EquipGame
{
    public class EquipGameState : AMiniGameState
    {
        private readonly Interacter _interacter;

        public EquipGameState(EquipGame game, Interacter interacter, EndGame endGame) : base(game, endGame) => 
            _interacter = interacter;

        public override void Enter()
        {
            _interacter.BindClicker();
            base.Enter();
        }

        public override void Exit()
        {
            base.Exit();
            _interacter.ExposeClicker();
        }
    }
}