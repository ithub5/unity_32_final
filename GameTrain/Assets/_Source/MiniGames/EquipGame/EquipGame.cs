using DG.Tweening;
using MiniGames.Common.Game;
using MiniGames.EquipGame.Data;
using Progression;
using Spawn;
using Utils.Services;

namespace MiniGames.EquipGame
{
    public class EquipGame : AMiniGame
    {
        private readonly EquipGameConfigSO _configSO;
        private readonly Spawner<HitPoint> _hitPoints;

        private Sequence _gameFlow;

        public EquipGame(EquipGameConfigSO configSO, Spawner<HitPoint> hitPoints, Scorer scorer, ProgressBar progressBar) :
            base(configSO, scorer, progressBar)
        {
            _configSO = configSO;
            _hitPoints = hitPoints;

            InitSequence();
        }

        public override void Start()
        {
            base.Start();
            
            _gameFlow.Restart();
        }

        public override void End()
        {
            base.End();
            
            _gameFlow.Pause();
            _hitPoints.DespawnActive();
        }

        private void ScoreDown() => scorer.Score(-_configSO.ScoreStep);

        private void AddHitPoint()
        {
            HitPoint newHitPoint = _hitPoints.Spawn();

            newHitPoint.OnHit += ScoreUp;
            newHitPoint.OnDie += ScoreDown;

            newHitPoint.OnDespawn += RemoveHitPoint;
        }

        private void RemoveHitPoint(HitPoint hitPoint)
        {
            hitPoint.OnHit -= ScoreUp;
            hitPoint.OnDie -= ScoreDown;
            
            hitPoint.OnDespawn -= RemoveHitPoint;
        }
        
        private void InitSequence()
        {
            _gameFlow = DOTweenService.GetAutoKillOffSequence();

            _gameFlow.AppendInterval(_configSO.SpawnDelay);
            _gameFlow.AppendCallback(AddHitPoint);
            _gameFlow.AppendCallback(() => _gameFlow.Restart());
        }
    }
}