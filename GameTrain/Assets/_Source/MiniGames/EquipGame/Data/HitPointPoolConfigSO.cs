using UnityEngine;
using Utils.Containers.Data;

namespace MiniGames.EquipGame.Data
{
    [CreateAssetMenu(menuName = "SO/EquipGame/HitPointPoolConfigSO", fileName = "NewHitPointPoolConfigSO")]
    public class HitPointPoolConfigSO : MonoPoolConfigSO<HitPoint> { }
}