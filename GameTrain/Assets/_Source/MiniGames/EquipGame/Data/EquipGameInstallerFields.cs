using System;
using UnityEngine;
using Utils.Data;

namespace MiniGames.EquipGame.Data
{
    [Serializable]
    public class EquipGameInstallerFields
    {
        [field: SerializeField] public EquipGameConfigSO ConfigSO {get; private set; }
        [field: SerializeField] public HitPointPoolConfigSO HitPointPoolConfigSO {get; private set; }
        [field: SerializeField] public TransformRange PlayArea {get; private set; }
    }
}