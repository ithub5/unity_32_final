using MiniGames.Common.Data;
using UnityEngine;

namespace MiniGames.EquipGame.Data
{
    [CreateAssetMenu(menuName = "SO/EquipGame/ConfigSO", fileName = "NewEquipGameConfigSO")]
    public class EquipGameConfigSO : AMiniGameConfigSO
    {
        [field: SerializeField] public float SpawnDelay { get; private set; }
    }
}