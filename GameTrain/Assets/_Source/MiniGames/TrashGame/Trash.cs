using System;
using DG.Tweening;
using Interactions.Click;
using UnityEngine;
using Utils.Services;
using Zenject;

namespace MiniGames.TrashGame
{
    public class Trash : MonoBehaviour, IClickable
    {
        [SerializeField] private float normalScale;
        [SerializeField] private float hitScale;
        [SerializeField] private float appearTime;
        [SerializeField] private float disappearTime;
        [SerializeField] private float hitScaleTime;
        [SerializeField] private float backToNormalScaleTime;

        private bool _interactable;
        
        private Sequence _spawnSequence;
        private Sequence _despawnSequence;
        private Sequence _hitSequence;
        
        public event Action OnHit;

        [Inject]
        private void Init() => InitSequence();

        public void Spawn() => _spawnSequence.Restart();
        
        public void Despawn() => _despawnSequence.Restart();

        private void Hit() => _hitSequence.Restart();
        
        public void StartedClick()
        {
            if (!_interactable)
                return;

            if (_hitSequence.IsPlaying())
                _hitSequence.GotoWithCallbacks(_hitSequence.Duration());
            
            Hit();
        }

        public void PerformedClick() { }
        public void CanceledClick() { }

        private void InitSequence()
        {
            _spawnSequence = DOTweenService.GetAutoKillOffSequence();

            _spawnSequence.AppendCallback(() => gameObject.SetActive(true));
            _spawnSequence.Append(transform.DOScale(normalScale, appearTime).From(0f));
            _spawnSequence.AppendCallback(() => _interactable = true);

            _despawnSequence = DOTweenService.GetAutoKillOffSequence();
            
            _despawnSequence.AppendCallback(() => _interactable = false);
            _despawnSequence.Append(transform.DOScale(0f, disappearTime));
            _despawnSequence.AppendCallback(() => gameObject.SetActive(false));

            _hitSequence = DOTweenService.GetAutoKillOffSequence();

            _hitSequence.Append(transform.DOScale(hitScale, hitScaleTime));
            _hitSequence.AppendCallback(() => OnHit?.Invoke());
            _hitSequence.Append(transform.DOScale(normalScale, backToNormalScaleTime));
        }
    }
}