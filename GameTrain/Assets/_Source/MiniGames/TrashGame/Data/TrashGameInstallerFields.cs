using System;
using UnityEngine;

namespace MiniGames.TrashGame.Data
{
    [Serializable]
    public class TrashGameInstallerFields
    {
        [field: SerializeField] public TrashGameConfigSO ConfigSO { get; private set; }
        [field: SerializeField] public Trash Trash { get; private set; }
    }
}