using MiniGames.Common.Data;
using UnityEngine;

namespace MiniGames.TrashGame.Data
{
    [CreateAssetMenu(menuName = "SO/TrashGame/ConfigSO", fileName = "NewTrashGameConfigSO")]
    public class TrashGameConfigSO : AMiniGameConfigSO { }
}