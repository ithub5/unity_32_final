using MiniGames.Common.Game;
using MiniGames.TrashGame.Data;
using Progression;

namespace MiniGames.TrashGame
{
    public class TrashGame : AMiniGame
    {
        private readonly Trash _trash;

        public TrashGame(TrashGameConfigSO configSO, Trash trash, Scorer scorer, ProgressBar progressBar) : 
            base(configSO, scorer, progressBar)
        {
            _trash = trash;
        }

        public override void Start()
        {
            base.Start();

            _trash.Spawn();
            _trash.OnHit += ScoreUp;
        }

        public override void End()
        {
            base.End();
            
            _trash.OnHit -= ScoreUp;
            _trash.Despawn();
        }
    }
}