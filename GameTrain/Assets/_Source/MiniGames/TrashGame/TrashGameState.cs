using Interactions;
using MiniGames.Common.Game;
using Progression;

namespace MiniGames.TrashGame
{
    public class TrashGameState : AMiniGameState
    {
        private readonly Interacter _interacter;

        public TrashGameState(TrashGame game, Interacter interacter, EndGame endGame) : base(game, endGame) => 
            _interacter = interacter;

        public override void Enter()
        {
            _interacter.BindClicker();
            base.Enter();
        }
        
        public override void Exit()
        {
            base.Exit();
            _interacter.ExposeClicker();
        }
    }
}