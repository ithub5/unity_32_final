using System;
using UnityEngine;

namespace MiniGames.SummonGame.Data
{
    [Serializable]
    public class SummonGameInstallerFields
    {
        [field: SerializeField] public SummonGameConfigSO ConfigSO { get; private set; }
        [field: SerializeField] public Key[] Keys { get; private set; }
        [field: SerializeField] public LockNView[] LocksNViews { get; private set; }
    }
}