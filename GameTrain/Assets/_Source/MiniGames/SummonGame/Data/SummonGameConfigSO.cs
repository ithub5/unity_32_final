using MiniGames.Common.Data;
using UnityEngine;

namespace MiniGames.SummonGame.Data
{
    [CreateAssetMenu(menuName = "SO/SummonGame/ConfigSO", fileName = "NewSummonGameConfigSO")]
    public class SummonGameConfigSO : AMiniGameConfigSO { }
}