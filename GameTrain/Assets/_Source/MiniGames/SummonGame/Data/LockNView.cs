using System;
using SpriteWorks;
using UnityEngine;

namespace MiniGames.SummonGame.Data
{
    [Serializable]
    public class LockNView
    {
        [field: SerializeField] public Lock Lock { get; private set; } 
        [field: SerializeField] public SpriteDefiner View { get; private set; } 
    }
}