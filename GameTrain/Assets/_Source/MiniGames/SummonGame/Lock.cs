using System;
using DG.Tweening;
using UnityEngine;
using Utils.Extensions;
using Utils.Services;
using Zenject;

namespace MiniGames.SummonGame
{
    public class Lock : MonoBehaviour
    {
        [SerializeField] private LayerMask keyMask;
        
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private float appearTime;
        [SerializeField] private float disappearTime;
        
        private Key _targetKey;
        private Key _currentKey;

        private Sequence _spawnSequence;
        private Sequence _despawnSequence;

        public event Action OnUndefined;
        public event Action OnCorrect;
        public event Action OnWrong;
        public event Action OnCorrectRemoved;

        [Inject]
        private void Init() => InitSequence();

        public void Spawn() => _spawnSequence.Restart();
        
        public void Despawn() => _despawnSequence?.Restart();
        
        public void SetUp(Key targetKey) => _targetKey = targetKey;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!keyMask.Contains(other.gameObject.layer) || !other.gameObject.TryGetComponent(out Key key))
                return;

            if (_currentKey != default)
                return;
            
            UseKey(key);
        }

        private void UseKey(Key key)
        {
            _currentKey = key;
            _currentKey?.Snap(this);
            
            ValidateKey(key);
        }

        public void RemoveKey()
        {
            UseKey(default);
                
            if (_currentKey != _targetKey) 
                return;
            
            OnCorrectRemoved?.Invoke();
        }

        private void ValidateKey(Key key)
        {
            if (key == default)
                OnUndefined?.Invoke();
            else if (key == _targetKey)
                OnCorrect?.Invoke();
            else
                OnWrong?.Invoke();
        }

        private void InitSequence()
        {
            _spawnSequence = DOTweenService.GetAutoKillOffSequence();

            _spawnSequence.AppendCallback(() => OnUndefined?.Invoke());
            _spawnSequence.AppendCallback(() => gameObject.SetActive(true));
            _spawnSequence.Append(spriteRenderer.DOFade(1f, appearTime).From(0f));

            _despawnSequence = DOTweenService.GetAutoKillOffSequence();

            _despawnSequence.Append(spriteRenderer.DOFade(0f, disappearTime));
            _despawnSequence.AppendCallback(() => gameObject.SetActive(false));
        }
    }
}