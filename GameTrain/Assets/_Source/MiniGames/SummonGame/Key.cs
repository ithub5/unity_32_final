using System;
using DG.Tweening;
using Interactions.Drag;
using UnityEngine;
using Utils.Services;
using Zenject;

namespace MiniGames.SummonGame
{
    public class Key : MonoBehaviour, IDraggable
    {
        [SerializeField] private float normalScale;
        [SerializeField] private float dragScale;
        [SerializeField] private float appearTime;
        [SerializeField] private float disappearTime;
        [SerializeField] private float dragScaleTime;
        [SerializeField] private float backToNormalScaleTime;
        
        private Vector3 _defaultPos;
        
        private bool _followDrag;

        private Lock _currentLock;
        private bool _isSnapped;

        private Sequence _spawnSequence;
        private Sequence _despawnSequence;
        private Sequence _startDragSequence;
        private Sequence _cancelDragSequence;

        [Inject]
        private void Init()
        {
            _defaultPos = transform.position;
            
            InitSequence();
        }

        public void Spawn() => _spawnSequence.Restart();
        
        public void Despawn() => _despawnSequence.Restart();

        public void Snap(Lock @lock)
        {
            _currentLock = @lock;
         
            transform.position = _currentLock.transform.position;
            _followDrag = false;
            _isSnapped = true;
        }

        private void Unsnap()
        {
            _currentLock.RemoveKey();
            _currentLock = default;
            
            _isSnapped = false;
        }

        public void StartedDrag(Vector2 startedPos)
        {
            if (_isSnapped)
                Unsnap();
            
            _startDragSequence.Restart();
        }

        public void PerformingDrag(Vector2 performingPos)
        {
            if (!_followDrag)
                return;

            Vector2 performingWorldPos = CameraService.ScreenToWorldPoint(performingPos);
            transform.position = performingWorldPos;
        }

        public void CanceledDrag(Vector2 canceledPos) => _cancelDragSequence.Restart();

        private void InitSequence()
        {
            _spawnSequence = DOTweenService.GetAutoKillOffSequence();

            _spawnSequence.AppendCallback(() => _followDrag = true);
            _spawnSequence.AppendCallback(() => transform.position = _defaultPos);
            _spawnSequence.AppendCallback(() => gameObject.SetActive(true));
            _spawnSequence.Append(transform.DOScale(normalScale, appearTime).From(0f));
            
            _despawnSequence = DOTweenService.GetAutoKillOffSequence();

            _despawnSequence.Append(transform.DOScale(0f, disappearTime).From(normalScale));
            _despawnSequence.AppendCallback(() => gameObject.SetActive(false));
            _despawnSequence.AppendCallback(Unsnap);

            _startDragSequence = DOTweenService.GetAutoKillOffSequence();

            _startDragSequence.AppendCallback(() => _followDrag = true);
            _startDragSequence.Append(transform.DOScale(dragScale, dragScaleTime));
            
            _cancelDragSequence = DOTweenService.GetAutoKillOffSequence();
            
            _cancelDragSequence.Append(transform.DOScale(normalScale, backToNormalScaleTime));
        }
    }
}