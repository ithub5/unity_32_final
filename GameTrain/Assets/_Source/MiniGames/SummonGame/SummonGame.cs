using System.Collections.Generic;
using MiniGames.Common.Game;
using MiniGames.SummonGame.Data;
using Progression;
using Utils.Extensions;
using Random = UnityEngine.Random;

namespace MiniGames.SummonGame
{
    public class SummonGame : AMiniGame
    {
        private readonly Key[] _keys;
        private readonly LockNView[] _locksNViews;
        
        public SummonGame(SummonGameConfigSO configSO, Key[] keys, LockNView[] locksNViews, Scorer scorer, ProgressBar progressBar) : 
            base(configSO, scorer, progressBar)
        {
            _keys = keys;
            _locksNViews = locksNViews;
        }

        public override void Start()
        { 
            base.Start();
            
            List<Key> tempKeys = new List<Key>(_keys);
            _keys.ForEach(Spawn);
            _locksNViews.ForEach(@lock => SetUpLock(@lock, tempKeys));
        }

        public override void End()
        {
            base.End();
            
            _keys.ForEach(Despawn);
            _locksNViews.ForEach(ResetLock);
        }
        
        private void ScoreDown() => scorer.Score(-configSO.ScoreStep);

        private void Spawn(Key key) => key.Spawn();

        private void Despawn(Key key) => key.Despawn();

        private void SetUpLock(LockNView lockNView, List<Key> keys)
        {
            lockNView.Lock.OnCorrect += ScoreUp;
            lockNView.Lock.OnCorrectRemoved += ScoreDown;

            lockNView.Lock.OnUndefined += lockNView.View.Undefined;
            lockNView.Lock.OnCorrect += lockNView.View.Correct;
            lockNView.Lock.OnWrong += lockNView.View.Wrong;
            
            int keyIndex = Random.Range(0, keys.Count);
            lockNView.Lock.SetUp(keys[keyIndex]);
            keys.RemoveAt(keyIndex);
                
            lockNView.Lock.Spawn();
        }

        private void ResetLock(LockNView lockNView)
        {
            lockNView.Lock.OnCorrect -= ScoreUp;
            lockNView.Lock.OnCorrectRemoved -= ScoreDown;
            
            lockNView.Lock.OnUndefined -= lockNView.View.Undefined;
            lockNView.Lock.OnCorrect -= lockNView.View.Correct;
            lockNView.Lock.OnWrong -= lockNView.View.Wrong;
            
            lockNView.Lock.Despawn();
        }
    }
}