using Interactions;
using MiniGames.Common.Game;
using Progression;

namespace MiniGames.SummonGame
{
    public class SummonGameState : AMiniGameState
    {
        private readonly Interacter _interacter;
        
        public SummonGameState(SummonGame game, Interacter interacter, EndGame endGame) : base(game, endGame) => 
            _interacter = interacter;

        public override void Enter()
        {
            _interacter.BindClicker();
            _interacter.BindDragger(); 
            base.Enter();
        }

        public override void Exit()
        {
            base.Exit();
            _interacter.ExposeClicker();
            _interacter.ExposeDragger();
        }
    }
}