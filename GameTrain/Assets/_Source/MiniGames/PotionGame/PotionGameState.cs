using Interactions;
using MiniGames.Common.Game;
using Progression;

namespace MiniGames.PotionGame
{
    public class PotionGameState : AMiniGameState
    {
        private readonly Interacter _interacter;

        public PotionGameState(PotionGame game, Interacter interacter, EndGame endGame) : base(game, endGame) => 
            _interacter = interacter;

        public override void Enter()
        {
            _interacter.BindClicker();
            base.Enter();
        }

        public override void Exit()
        {
            base.Exit();
            _interacter.ExposeClicker();
        }
    }
}