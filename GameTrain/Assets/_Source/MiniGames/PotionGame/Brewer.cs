using System;
using Animations;
using DG.Tweening;
using Interactions.Click;
using UnityEngine;
using Utils.Services;
using Zenject;

namespace MiniGames.PotionGame
{
    public class Brewer : MonoBehaviour, IClickable
    {
        [SerializeField] private LayerMask potionPartMask;
        [SerializeField] private CircleCollider2D collectCollider;
        [SerializeField] private Mover mover;
        
        [SerializeField] private float normalScale;
        [SerializeField] private float collectScale;
        [SerializeField] private float appearTime;
        [SerializeField] private float disappearTime;
        [SerializeField] private float collectScaleTime;
        [SerializeField] private float backToNormalScaleTime;

        private Sequence _spawnSequence;
        private Sequence _despawnSequence;
        private Sequence _collectSequence;
        
        public event Action OnMiss;

        [Inject]
        private void Init() => InitSequence();

        public void Spawn() => _spawnSequence.Restart();
        
        public void Despawn() => _despawnSequence.Restart();

        public void StartMove()
        {
            mover.PingPongOn();
            mover.MoveToUpperRight();
        }

        public void StopMove()
        {
            mover.Stop();
            mover.PingPongOff();
        }

        public void StartedClick() => TryCollect();

        public void PerformedClick() { }

        public void CanceledClick() { }
        
        private void TryCollect()
        {
            _collectSequence.Restart();
            
            Collider2D collectionResult = 
                Physics2D.OverlapCircle(collectCollider.transform.position, collectCollider.radius, potionPartMask);

            if (collectionResult == null || !collectionResult.TryGetComponent(out PotionPart potionPart))
            {
                OnMiss?.Invoke();
                return;
            }

            potionPart.Collect();
        }

        private void InitSequence()
        {
            _spawnSequence = DOTweenService.GetAutoKillOffSequence();

            _spawnSequence.AppendCallback(() => gameObject.SetActive(true));
            _spawnSequence.Append(transform.DOScale(normalScale, appearTime).From(0f));
            
            _despawnSequence = DOTweenService.GetAutoKillOffSequence();

            _despawnSequence.Append(transform.DOScale(0f, disappearTime).From(normalScale));
            _despawnSequence.AppendCallback(() => gameObject.SetActive(false));

            _collectSequence = DOTweenService.GetAutoKillOffSequence();

            _collectSequence.Append(transform.DOScale(collectScale, collectScaleTime));
            _collectSequence.Append(transform.DOScale(normalScale, backToNormalScaleTime));
        }
    }
}