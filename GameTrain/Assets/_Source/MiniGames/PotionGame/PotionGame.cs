using DG.Tweening;
using MiniGames.Common.Game;
using MiniGames.PotionGame.Data;
using Progression;
using Spawn;
using UnityEngine;
using Utils.Services;

namespace MiniGames.PotionGame
{
    public class PotionGame : AMiniGame
    {
        private readonly PotionGameConfigSO _configSO;
        private readonly Brewer _brewer;
        private readonly Spawner<PotionPart> _potionParts;

        private int _potionPartsPlacedAtStart;
        private Sequence _gameStart;
        
        public PotionGame(PotionGameConfigSO configSO, Brewer brewer, Spawner<PotionPart> potionParts, 
            Scorer scorer, ProgressBar progressBar) : base(configSO, scorer, progressBar)
        {
            _configSO = configSO;
            _brewer = brewer;
            _potionParts = potionParts;
            
            InitSequence();
        }

        public override void Start()
        {
            base.Start();
            
            _brewer.Spawn();
            _brewer.StartMove();
            _brewer.OnMiss += ScoreDown;
            
            _gameStart.Restart();
        }

        public override void End()
        {
            base.End();
            
            _brewer.StopMove();
            _brewer.Despawn();
            _brewer.OnMiss -= ScoreDown;
            
            _potionParts.DespawnActive();
            _potionPartsPlacedAtStart = 0;
        }

        private void ScoreDown() => scorer.Score(-configSO.ScoreStep);

        private void AddPotionPart()
        {
            PotionPart newPotionPart = _potionParts.Spawn();

            newPotionPart.OnCollect += AddPotionPart;
            newPotionPart.OnCollect += ScoreUp;

            newPotionPart.OnDespawn += RemovePotionPart;
        }

        private void RemovePotionPart(PotionPart potionPart)
        {
            potionPart.OnCollect -= AddPotionPart;
            potionPart.OnCollect -= ScoreUp;

            potionPart.OnDespawn -= RemovePotionPart;
        }

        private void InitSequence()
        {
            _gameStart = DOTweenService.GetAutoKillOffSequence();

            _gameStart.AppendInterval(Time.fixedDeltaTime);
            _gameStart.AppendCallback(AddPotionPart);
            _gameStart.AppendCallback(RepeatGameStart);
        }

        private void RepeatGameStart()
        {
            _potionPartsPlacedAtStart++;
            if (_potionPartsPlacedAtStart >= _configSO.ActivePotionParts)
                return;
            
            _gameStart.Restart();
        }
    }
}