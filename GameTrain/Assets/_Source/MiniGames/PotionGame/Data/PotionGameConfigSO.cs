using MiniGames.Common.Data;
using UnityEngine;

namespace MiniGames.PotionGame.Data
{
    [CreateAssetMenu(menuName = "SO/PotionGame/ConfigSO", fileName = "NewPotionGameConfigSO")]
    public class PotionGameConfigSO : AMiniGameConfigSO
    {
        [field: SerializeField] public int ActivePotionParts { get; private set; }
    }
}