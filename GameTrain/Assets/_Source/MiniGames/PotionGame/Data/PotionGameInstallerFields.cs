using System;
using UnityEngine;
using Utils.Data;

namespace MiniGames.PotionGame.Data
{
    [Serializable]
    public class PotionGameInstallerFields
    {
        [field: SerializeField] public PotionGameConfigSO ConfigSO {get; private set; }
        [field: SerializeField] public PotionPartPoolConfigSO PotionPartPoolConfigSO {get; private set; }
        [field: SerializeField] public Brewer Brewer {get; private set; }
        [field: SerializeField] public TransformRange PlayArea {get; private set; }
    }
}