using UnityEngine;
using Utils.Containers.Data;

namespace MiniGames.PotionGame.Data
{
    [CreateAssetMenu(menuName = "SO/PotionGame/PotionPartPoolConfigSO", fileName = "NewPotionPartPoolConfigSO")]
    public class PotionPartPoolConfigSO : MonoPoolConfigSO<PotionPart> { }
}