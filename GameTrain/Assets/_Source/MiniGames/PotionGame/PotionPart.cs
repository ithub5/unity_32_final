using System;
using DG.Tweening;
using Spawn;
using UnityEngine;
using Utils.Data;
using Utils.Services;

namespace MiniGames.PotionGame
{
    public class PotionPart : MonoBehaviour, ISpawnable<PotionPart>
    {
        [SerializeField] private CircleCollider2D placeCollider;
        [SerializeField] private LayerMask placeMask;

        [SerializeField] private float normalScale;
        [SerializeField] private float collectScale;
        [SerializeField] private float appearTime;
        [SerializeField] private float disappearTime;
        [SerializeField] private float collectScaleTime;
        
        private Sequence _spawnSequence;
        private Sequence _despawnSequence;
        private Sequence _collectSequence;

        public event Action OnCollect; 
        public event Action<PotionPart> OnDespawn;

        private void Awake() => InitSequence();

        public void Spawn() => _spawnSequence.Restart();

        public void Despawn() => _despawnSequence.Restart();

        public void Collect() => _collectSequence.Restart();

        public void PlaceSelf(TransformRange placeRange)
        {
            transform.position = placeRange.RandomInRange();
            
            if (Physics2D.OverlapCircle(placeCollider.transform.position, placeCollider.radius, placeMask) == null)
                return;
            
            PlaceSelf(placeRange);
        }

        private void InitSequence()
        {
            _spawnSequence = DOTweenService.GetAutoKillOffSequence();

            _spawnSequence.AppendCallback(() => gameObject.SetActive(true));
            _spawnSequence.Append(transform.DOScale(Vector3.one * normalScale, appearTime).From(Vector3.zero));

            _despawnSequence = DOTweenService.GetAutoKillOffSequence();
            
            _despawnSequence.Append(transform.DOScale(Vector3.zero, disappearTime));
            _despawnSequence.AppendCallback(() => gameObject.SetActive(false));
            _despawnSequence.AppendCallback(() => OnDespawn?.Invoke(this));

            _collectSequence = DOTweenService.GetAutoKillOffSequence();

            _collectSequence.Append(transform.DOScale(collectScale, collectScaleTime));
            _collectSequence.AppendCallback(() => OnCollect?.Invoke());
            _collectSequence.AppendCallback(Despawn);
        }
    }
}