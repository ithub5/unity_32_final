using System;
using DG.Tweening;
using Interactions.Drag;
using Spawn;
using UnityEngine;
using Utils.Data;
using Utils.Services;

namespace MiniGames.ExperienceGame
{
    public class ExperiencePart : MonoBehaviour, IDraggable, ISpawnable<ExperiencePart>
    {
        [SerializeField] private float launchForce;
        [SerializeField] private Rigidbody2D rb;

        [SerializeField] private CircleCollider2D placeCollider;
        [SerializeField] private LayerMask placeMask;

        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private float appearTime;
        [SerializeField] private float disappearTime;
        [SerializeField] private Transform directionArrow;

        private bool _interactable;
        private Vector2 _startDragPos;

        private Sequence _spawnSequence;
        private Sequence _despawnSequence;
        
        public event Action<ExperiencePart> OnDespawn;

        private void Awake() => InitSequence();

        public void Spawn() => _spawnSequence.Restart();

        public void Despawn() => _despawnSequence.Restart();
        
        public void Collect() => Despawn();

        public void StartedDrag(Vector2 startedPos)
        {
            if (!_interactable) 
                return;
            
            _startDragPos = startedPos;
            directionArrow.gameObject.SetActive(true);
        }

        public void PerformingDrag(Vector2 performingPos)
        {
            float angle = Mathf.Atan2(performingPos.y - _startDragPos.y , performingPos.x - _startDragPos.x) * Mathf.Rad2Deg;
            directionArrow.eulerAngles = Vector3.forward * (angle + -90f);
        }

        public void CanceledDrag(Vector2 canceledPos)
        {
            Vector2 launchVector = (canceledPos - _startDragPos).normalized * launchForce;

            rb.AddForce(launchVector, ForceMode2D.Impulse);
            directionArrow.gameObject.SetActive(false);
        }

        public void PlaceSelf(TransformRange placeRange)
        {
            transform.position = placeRange.RandomInRange();
            
            if (Physics2D.OverlapCircle(placeCollider.transform.position, placeCollider.radius, placeMask) == null)
                return;
            
            PlaceSelf(placeRange);
        }

        private void InitSequence()
        {
            _spawnSequence = DOTweenService.GetAutoKillOffSequence();

            _spawnSequence.AppendCallback(() => gameObject.SetActive(true));
            _spawnSequence.Append(spriteRenderer.DOFade(1f, appearTime).From(0f));
            _spawnSequence.AppendCallback(() => _interactable = true);

            _despawnSequence = DOTweenService.GetAutoKillOffSequence();

            _despawnSequence.AppendCallback(() => rb.velocity = Vector2.zero);
            _despawnSequence.AppendCallback(() => _interactable = false);
            _despawnSequence.Append(spriteRenderer.DOFade(0f, disappearTime));
            _despawnSequence.AppendCallback(() => gameObject.SetActive(false));
            _despawnSequence.AppendCallback(() => OnDespawn?.Invoke(this));
        }
    }
}