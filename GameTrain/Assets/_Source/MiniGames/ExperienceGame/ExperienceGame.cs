using DG.Tweening;
using MiniGames.Common.Game;
using MiniGames.ExperienceGame.Data;
using Progression;
using Spawn;
using UnityEngine;
using Utils.Services;

namespace MiniGames.ExperienceGame
{
    public class ExperienceGame : AMiniGame
    {
        private readonly ExperienceGameConfigSO _configSO;
        private readonly Spawner<ExperiencePart> _experienceParts;
        private readonly ExperienceBottle _experienceBottle;

        private int _targetExperienceParts;
        private int _currentExperienceParts;
        private Sequence _gameStart;

        public ExperienceGame(ExperienceGameConfigSO configSO, Spawner<ExperiencePart> experienceParts, 
            ExperienceBottle experienceBottle, Scorer scorer, ProgressBar progressBar) : 
            base(configSO, scorer, progressBar)
        {
            _experienceParts = experienceParts;
            _experienceBottle = experienceBottle;
            
            InitSequence();
        }

        public override void Start()
        {
            base.Start();
            
            _experienceBottle.Spawn();
            _experienceBottle.PlaceSelf();
            _experienceBottle.OnAdd += ScoreUp;
            _targetExperienceParts = scorer.TargetValue;
            
            _gameStart.Restart();
        }

        public override void End()
        {
            base.End();

            _experienceBottle.Despawn();
            _experienceBottle.OnAdd -= ScoreUp;
            _experienceParts.DespawnActive();
            _currentExperienceParts = 0;
        }

        private void AddExperiencePart() => _experienceParts.Spawn();

        private void InitSequence()
        {
            _gameStart = DOTweenService.GetAutoKillOffSequence();

            _gameStart.AppendInterval(Time.fixedDeltaTime);
            _gameStart.AppendCallback(AddExperiencePart);
            _gameStart.AppendCallback(RepeatGameStart);
        }

        private void RepeatGameStart()
        {
            _currentExperienceParts++;
            if (_currentExperienceParts >= _targetExperienceParts)
                return;

            _gameStart.Restart();
        }
    }
}