using MiniGames.Common.Data;
using UnityEngine;

namespace MiniGames.ExperienceGame.Data
{
    [CreateAssetMenu(menuName = "SO/ExperienceGame/ConfigSO", fileName = "NewExperienceGameConfigSO")]
    public class ExperienceGameConfigSO : AMiniGameConfigSO { }
}