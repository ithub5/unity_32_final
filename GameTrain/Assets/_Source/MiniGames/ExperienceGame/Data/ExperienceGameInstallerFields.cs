using System;
using UnityEngine;
using Utils.Data;

namespace MiniGames.ExperienceGame.Data
{
    [Serializable]
    public class ExperienceGameInstallerFields
    {
        [field: SerializeField] public ExperienceGameConfigSO ConfigSO { get; private set; }
        [field: SerializeField] public ExperiencePartPoolConfigSO ExperiencePartPoolConfigSO { get; private set; }
        [field: SerializeField] public ExperienceBottle ExperienceBottle { get; private set; }
        [field: SerializeField] public TransformRange PlayArea { get; private set; }
    }
}