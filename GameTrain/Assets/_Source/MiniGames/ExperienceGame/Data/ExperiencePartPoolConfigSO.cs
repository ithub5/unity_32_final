using UnityEngine;
using Utils.Containers.Data;

namespace MiniGames.ExperienceGame.Data
{
    [CreateAssetMenu(menuName = "SO/ExperienceGame/ExperiencePartPoolConfigSO", fileName = "NewExperiencePartPoolConfigSO")]
    public class ExperiencePartPoolConfigSO : MonoPoolConfigSO<ExperiencePart> { }
}