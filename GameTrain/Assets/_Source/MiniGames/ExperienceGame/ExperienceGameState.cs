using Interactions;
using MiniGames.Common.Game;
using Progression;

namespace MiniGames.ExperienceGame
{
    public class ExperienceGameState : AMiniGameState
    {
        private readonly Interacter _interacter;

        public ExperienceGameState(ExperienceGame game, Interacter interacter, EndGame endGame) : base(game, endGame) => 
            _interacter = interacter;

        public override void Enter()
        {
            _interacter.BindClicker();
            _interacter.BindDragger();
            base.Enter();
        }

        public override void Exit()
        {
            base.Exit();
            _interacter.ExposeClicker();
            _interacter.ExposeDragger();
        }
    }
}