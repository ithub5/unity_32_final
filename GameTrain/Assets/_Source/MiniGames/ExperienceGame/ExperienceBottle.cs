using System;
using DG.Tweening;
using UnityEngine;
using Utils.Data;
using Utils.Extensions;
using Utils.Services;
using Zenject;

namespace MiniGames.ExperienceGame
{
    public class ExperienceBottle : MonoBehaviour
    {
        [SerializeField] private LayerMask experiencePartMask;

        [SerializeField] private TransformRange placeRange;
        [SerializeField] private CircleCollider2D placeCollider;
        [SerializeField] private LayerMask placeMask;

        [SerializeField] private float normalScale;
        [SerializeField] private float collectScale;
        [SerializeField] private float appearTime;
        [SerializeField] private float disappearTime;
        [SerializeField] private float collectScaleTime;
        [SerializeField] private float backToNormalScaleTime;
        
        private Sequence _spawnSequence;
        private Sequence _despawnSequence;
        private Sequence _collectSequence;
        
        public event Action OnAdd;

        [Inject]
        private void Init() => InitSequence();

        public void Spawn() => _spawnSequence.Restart();

        public void Despawn() => _despawnSequence.Restart();

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!experiencePartMask.Contains(other.gameObject.gameObject.layer) || !other.TryGetComponent(out ExperiencePart experiencePart))
                return;

            experiencePart.Collect();
            Collect();
        }

        private void Collect()
        {
            if (_collectSequence.IsPlaying())
                _collectSequence.GotoWithCallbacks(_collectSequence.Duration());

            _collectSequence.Restart();
        }

        public void PlaceSelf()
        {
            transform.position = placeRange.RandomInRange();
            
            if (Physics2D.OverlapCircle(placeCollider.transform.position, placeCollider.radius, placeMask) == null)
                return;
            
            PlaceSelf();
        }

        private void InitSequence()
        {
            _spawnSequence = DOTweenService.GetAutoKillOffSequence();

            _spawnSequence.AppendCallback(() => gameObject.SetActive(true));
            _spawnSequence.Append(transform.DOScale(normalScale, appearTime).From(0f));
            
            _despawnSequence = DOTweenService.GetAutoKillOffSequence();

            _despawnSequence.Append(transform.DOScale(0f, disappearTime).From(normalScale));
            _despawnSequence.AppendCallback(() => gameObject.SetActive(false));

            _collectSequence = DOTweenService.GetAutoKillOffSequence();

            _collectSequence.Append(transform.DOScale(collectScale, collectScaleTime));
            _collectSequence.Append(transform.DOScale(normalScale, backToNormalScaleTime));
            _collectSequence.AppendCallback(() => OnAdd?.Invoke());
        }
    }
}