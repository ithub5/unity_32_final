using Input;
using Interactions.Data;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils.Services;
using Zenject;

namespace Interactions.Click
{
    public class Clicker
    {
        private readonly MainActions _input;

        private readonly LayerMask _targetMask;
        
        private IClickable _currentClickable;
        
        public Clicker(MainActions input, [Inject(Id = ZInteractionIDs.ClickableMask)] LayerMask targetMask)
        {
            _input = input;
            _targetMask = targetMask;
        }
        
        public void Bind()
        {
            _input.Interactions.LMB.started += Click;
            _input.Interactions.LMB.performed += Click;
            _input.Interactions.LMB.canceled += Click;
        }

        public void Expose()
        {
            _input.Interactions.LMB.started -= Click;
            _input.Interactions.LMB.performed -= Click;
            _input.Interactions.LMB.canceled -= Click;

            _currentClickable = default;
        }
        
        private void Click(InputAction.CallbackContext ctx)
        {
            Vector2 mousePos = _input.Interactions.MousePos.ReadValue<Vector2>();
            
            switch (ctx.phase)
            {
                case InputActionPhase.Started:
                    if (!RayCastService.RayCastFromScreen(mousePos, _targetMask, out RaycastHit2D targetClickable))
                        break;
                    
                    _currentClickable = targetClickable.rigidbody.GetComponent<IClickable>();
                    _currentClickable.StartedClick();
                    break;
                case InputActionPhase.Performed:
                    if (!RayCastService.RayCastFromScreen(mousePos, _targetMask, out targetClickable) || 
                        targetClickable.collider.GetComponent<IClickable>() != _currentClickable)
                        break;
                    
                    _currentClickable?.PerformedClick();
                    break;
                case InputActionPhase.Canceled:
                    if (!RayCastService.RayCastFromScreen(mousePos, _targetMask, out targetClickable) || 
                        targetClickable.collider.GetComponent<IClickable>() != _currentClickable)
                        break;
                    
                    _currentClickable?.CanceledClick();
                    _currentClickable = default;
                    break;
            }
        }
    }
}