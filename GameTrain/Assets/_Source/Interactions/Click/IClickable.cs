namespace Interactions.Click
{
    public interface IClickable
    {
        public void StartedClick();
        public void PerformedClick();
        public void CanceledClick();
    }
}