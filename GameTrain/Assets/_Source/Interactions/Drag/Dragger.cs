using Input;
using Interactions.Data;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils.Services;
using Zenject;

namespace Interactions.Drag
{
    public class Dragger
    {
        private MainActions _input;

        private readonly LayerMask _targetMask;

        private IDraggable _currentDraggable;

        public Dragger(MainActions input, [Inject(Id = ZInteractionIDs.SwipeableMask)] LayerMask targetMask)
        {
            _input = input;
            _targetMask = targetMask;
        }
        
        public void Bind()
        {
            _input.Interactions.LMB.started += Drag;
            _input.Interactions.MousePos.performed += Drag;
            _input.Interactions.LMB.canceled += Drag;
        }

        public void Expose()
        {
            _input.Interactions.LMB.started -= Drag;
            _input.Interactions.MousePos.performed -= Drag;
            _input.Interactions.LMB.canceled -= Drag;
            
            _currentDraggable = default;
        }
        
        private void Drag(InputAction.CallbackContext ctx)
        {
            Vector2 mousePos = _input.Interactions.MousePos.ReadValue<Vector2>();
            switch (ctx.phase)
            {
                case InputActionPhase.Started:
                    if (!RayCastService.RayCastFromScreen(mousePos, _targetMask, out RaycastHit2D targetClickable))
                        break;
                    _currentDraggable = targetClickable.rigidbody.GetComponent<IDraggable>();
                    _currentDraggable.StartedDrag(mousePos);
                    break;
                case InputActionPhase.Performed:
                    _currentDraggable?.PerformingDrag(mousePos);
                    break;
                case InputActionPhase.Canceled:
                    _currentDraggable?.CanceledDrag(mousePos);
                    
                    _currentDraggable = default;
                    break;
            }
        }
    }
}