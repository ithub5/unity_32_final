using UnityEngine;

namespace Interactions.Drag
{
    public interface IDraggable
    {
        public void StartedDrag(Vector2 startedPos);
        public void PerformingDrag(Vector2 performingPos);
        public void CanceledDrag(Vector2 canceledPos);
    }
}