namespace Interactions.Data
{
    public enum ZInteractionIDs
    {
        ClickableMask = 0,
        SwipeableMask = 1
    }
}