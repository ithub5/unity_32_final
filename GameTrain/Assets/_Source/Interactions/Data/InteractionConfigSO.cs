using UnityEngine;

namespace Interactions.Data
{
    [CreateAssetMenu(menuName = "SO/Interaction/ConfigSO", fileName = "NewInteractionConfigSO")]
    public class InteractionConfigSO : ScriptableObject
    {
        [field: SerializeField] public LayerMask ClickableMask { get; private set; }
        [field: SerializeField] public LayerMask SwipeableMask { get; private set; }
    }
}