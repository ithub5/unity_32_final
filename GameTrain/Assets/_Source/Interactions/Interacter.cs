using Interactions.Click;
using Interactions.Drag;

namespace Interactions
{
    public class Interacter
    {
        private readonly Clicker _clicker;
        private readonly Dragger _dragger;

        public Interacter(Clicker clicker, Dragger dragger)
        {
            _clicker = clicker;
            _dragger = dragger;
        }

        public void BindClicker() => _clicker.Bind();
        
        public void ExposeClicker() => _clicker.Expose();
        
        public void BindDragger() => _dragger.Bind();
        
        public void ExposeDragger() => _dragger.Expose();
    }
}