using UnityEngine;
using UnityEngine.UI;

namespace UI.Lose
{
    public class LoseUIView : MonoBehaviour
    {
        [SerializeField] private GameObject panel;
        
        [field: SerializeField] public Button RestartBtn { get; private set; } 
        [field: SerializeField] public Button MenuBtn { get; private set; }

        public void ToggleOn() => panel.SetActive(true);
        public void ToggleOff() => panel.SetActive(false);
    }
}