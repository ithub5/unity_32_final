using System;
using UnityEngine;

namespace UI.Loot.Data
{
    [Serializable]
    public class LootUIInstallerFields
    {
        [field: SerializeField] public LootPileUIView LootPileUIView { get; private set; }
    }
}