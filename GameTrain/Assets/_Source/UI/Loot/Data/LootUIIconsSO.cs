using System.Collections.Generic;
using Loot.Data;
using UnityEngine;

namespace UI.Loot.Data
{
    [CreateAssetMenu(menuName = "SO/UI/Loot/UIIconsSO", fileName = "NewLootUIIconsSO")]
    public class LootUIIconsSO : ScriptableObject
    {
        [SerializeField] private List<Sprite> lootUIIcons;
        
        public Sprite this[LootType lootType] => lootUIIcons[(int)lootType];
    }
}