using Loot;

namespace UI.Loot
{
    public class LootPileUIController
    {
        private readonly LootPile _model;
        private readonly LootPileUIView _view;

        public LootPileUIController(LootPile model, LootPileUIView view)
        {
            _model = model;
            _view = view;

            Bind();
        }

        private void Bind()
        {
            _model.OnLootUpdate += _view.UpdateView;
        }
    }
}