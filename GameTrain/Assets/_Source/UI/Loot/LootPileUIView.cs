using System.Collections.Generic;
using Loot.Data;
using UI.Loot.Data;
using UnityEngine;
using Utils.Services;

namespace UI.Loot
{
    public class LootPileUIView : MonoBehaviour
    {
        [SerializeField] private LootTypeUIView lootTypeUIPrefab;
        [SerializeField] private Transform lootTypeUIParent;
        [SerializeField] private LootUIIconsSO lootUIIconsSO;

        private readonly Dictionary<LootType, LootTypeUIView> _lootTypeUIs = new Dictionary<LootType, LootTypeUIView>();

        private void Awake() => EnumService.ForEach<LootType>(AddLootTypeText);

        public void UpdateView(LootType lootType, int newAmount) => _lootTypeUIs[lootType].UpdateAmount(newAmount);

        private void AddLootTypeText(LootType lootType)
        {
            LootTypeUIView newLootTypeUIView = Instantiate(lootTypeUIPrefab.gameObject, lootTypeUIParent).GetComponent<LootTypeUIView>();
            newLootTypeUIView.UpdateAmount(0);
            newLootTypeUIView.UpdateIcon(lootUIIconsSO[lootType]);
            
            _lootTypeUIs.Add(lootType, newLootTypeUIView);
        }
    }
}