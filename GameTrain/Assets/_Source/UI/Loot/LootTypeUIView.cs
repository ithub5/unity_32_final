using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Loot
{
    public class LootTypeUIView : MonoBehaviour
    {
        [SerializeField] private Image lootIcon;
        [SerializeField] private TextMeshProUGUI lootAmount;

        [SerializeField] private float updateFontSizeEnlargement;
        [SerializeField] private float toUpdateFontSizeTime;
        [SerializeField] private float toNormalFontSizeTime;

        private float _normalSizeFontSize;
        
        private Sequence _updateAmountSequence;

        public void UpdateAmount(int newAmount)
        {
            lootAmount.text = newAmount.ToString();
            PlaySequence();
        }

        public void UpdateIcon(Sprite newIcon) => lootIcon.sprite = newIcon;

        private void PlaySequence()
        {
            _updateAmountSequence = DOTween.Sequence();
            _normalSizeFontSize = lootAmount.fontSize;

            _updateAmountSequence.AppendCallback(() => lootAmount.enableAutoSizing = false);
            _updateAmountSequence.Append(DOTween.To(() => lootAmount.fontSize, value => lootAmount.fontSize = value,
                _normalSizeFontSize + updateFontSizeEnlargement, toUpdateFontSizeTime));
            _updateAmountSequence.Append(DOTween.To(() => lootAmount.fontSize, value => lootAmount.fontSize = value,
                _normalSizeFontSize, toNormalFontSizeTime));
            _updateAmountSequence.AppendCallback(() => lootAmount.enableAutoSizing = true);
        }
    }
}