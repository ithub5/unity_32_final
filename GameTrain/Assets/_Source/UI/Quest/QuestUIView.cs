using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DG.Tweening;
using Loot.Data;
using TMPro;
using UI.Loot;
using UI.Loot.Data;
using UnityEngine;
using UnityEngine.UI;
using Utils.Services;
using Zenject;

namespace UI.Quest
{
    public class QuestUIView : MonoBehaviour
    {
        [SerializeField] private RectTransform questPanel;
        [SerializeField] private LootTypeUIView lootTypeUIPrefab;
        [SerializeField] private Transform lootTypeUIsParent;
        [SerializeField] private LootUIIconsSO lootUIIconsSO;
        [SerializeField] private Slider timer;

        [SerializeField] private TextMeshProUGUI questCompletedText;

        [SerializeField] private RectTransform questPanelShowPivotPoint;
        [SerializeField] private RectTransform questPanelHidePivotPoint;
        [SerializeField] private float questPanelAppearTime;
        [SerializeField] private float questPanelDisappearTime;

        private readonly Dictionary<LootType, LootTypeUIView> _lootTypeUIs = new Dictionary<LootType, LootTypeUIView>();

        private ReadOnlyDictionary<LootType, int> _lootNeeded;
        private float _timeToComplete;
        private float _timeLeft;

        private Sequence _updateQuestSequence;

        public event Action OnQuestUpdated; 

        [Inject]
        private void Init()
        {
            EnumService.ForEach<LootType>(AddLootTypeText);
            
            InitSequence();
        }

        public void UpdateCompleted(int newAmount) => questCompletedText.text = newAmount.ToString();

        public void UpdateQuest(ReadOnlyDictionary<LootType, int> lootNeeded, float timeToComplete)
        {
            _lootNeeded = lootNeeded;
            _timeToComplete = timeToComplete;

            _updateQuestSequence.Restart();
        }

        private void UpdateQuestValues()
        {
            EnumService.ForEach<LootType>(lootType =>
            {
                if (!_lootNeeded.ContainsKey(lootType))
                {
                    HideLoot(lootType);
                    return;
                }
            
                ShowLoot(lootType, _lootNeeded[lootType]);
            });
            
            SetTimer();
        }

        private void ShowLoot(LootType lootType, int amount)
        {
            _lootTypeUIs[lootType].UpdateAmount(amount);
            _lootTypeUIs[lootType].gameObject.SetActive(true);
        }

        private void HideLoot(LootType lootType) => _lootTypeUIs[lootType].gameObject.SetActive(false);

        private void SetTimer()
        {
            StopAllCoroutines();
            
            _timeLeft = _timeToComplete;
            StartCoroutine(CountTime());
        }
        
        private IEnumerator CountTime()
        {
            while (_timeLeft > 0)
            {
                yield return new WaitForEndOfFrame();
                _timeLeft -= Time.deltaTime;

                timer.value = _timeLeft / _timeToComplete;
            }
        }
        
        private void AddLootTypeText(LootType lootType)
        {
            LootTypeUIView newLootTypeUIView = Instantiate(lootTypeUIPrefab.gameObject, lootTypeUIsParent).GetComponent<LootTypeUIView>();
            newLootTypeUIView.UpdateAmount(0);
            newLootTypeUIView.UpdateIcon(lootUIIconsSO[lootType]);
            newLootTypeUIView.gameObject.SetActive(false);
            
            _lootTypeUIs.Add(lootType, newLootTypeUIView);
        }

        private void InitSequence()
        {
            _updateQuestSequence = DOTweenService.GetAutoKillOffSequence();

            // Debug.Log(questPanel.anchoredPosition);
            // Debug.Log(questPanelHidePivotPoint.anchoredPosition);
            
            _updateQuestSequence.Append(questPanel.DOAnchorPos(questPanelHidePivotPoint.anchoredPosition, questPanelDisappearTime));
            _updateQuestSequence.AppendCallback(UpdateQuestValues);
            _updateQuestSequence.Append(questPanel.DOAnchorPos(questPanelShowPivotPoint.anchoredPosition, questPanelAppearTime));
            _updateQuestSequence.AppendCallback(() => OnQuestUpdated?.Invoke());
        }
    }
}