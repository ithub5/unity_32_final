using System;
using UnityEngine;

namespace UI.Quest.Data
{
    [Serializable]
    public class QuestSystemUIInstallerFields
    {
        [field: SerializeField] public QuestUIView QuestUIView { get; private set; } 
    }
}