namespace UI.Quest
{
    public class QuestUIController
    {
        private readonly QuestSystem.Quest _model;
        private readonly QuestUIView _view;

        public QuestUIController(QuestSystem.Quest model, QuestUIView view)
        {
            _model = model;
            _view = view;

            Bind();
        }

        private void Bind()
        {
            _model.OnComplete += UpdateCompleted;
            _model.OnNewTarget += UpdateQuest;

            _view.OnQuestUpdated += StartQuest;
        }
        
        private void UpdateCompleted() => _view.UpdateCompleted(_model.CompletedAmount);

        private void UpdateQuest() => _view.UpdateQuest(_model.LootNeeded, _model.TimeToComplete);
        
        private void StartQuest() => _model.Start();
    }
}