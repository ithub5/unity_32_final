using System;
using UnityEngine;

namespace UI.MainMenu.Data
{
    [Serializable]
    public class MainMenuUIInstallerFields
    {
        [field: SerializeField] public MainMenuUIView MainMenuUIView { get; private set; }
    }
}