using UnityEngine.SceneManagement;

namespace UI.MainMenu
{
    public class MainMenuUIController
    {
        private readonly MainMenuUIView _view;

        public MainMenuUIController(MainMenuUIView view)
        {
            _view = view;

            Bind();
        }

        private void Bind() => _view.PlayBtn.onClick.AddListener(() => SceneManager.LoadScene("GamePlay"));
    }
}